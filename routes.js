'use strict';

const auth = require('basic-auth');
const jwt = require('jsonwebtoken');

const register = require('./functions/register');
const login = require('./functions/login');
const logout = require('./functions/logout');
const profile = require('./functions/profile');
const password = require('./functions/password');
const config = require('./config/config.json');
module.exports = router => {

	router.get('/', (req, res) => res.end('Welcome to xBorder !'));

	router.post('/authenticate', (req, res) => {

		const credentials = auth(req);

		if (!credentials) {

			res.status(400).json({ message: 'Invalid Request !' });

		} else {

			login.loginUser(credentials.name, credentials.pass)
			.then(result => {
				const r = result;
				const msg = r.message
				console.log(r)
				//strip out the name part and only keep the email for token
				result.message = r.message.split(":")[0]
				const token = jwt.sign(result, config.secret, { expiresIn: 1440 });
				console.log(r)
				console.log(msg)
				res.status(result.status).json({ message: msg, token: token });

			})

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});

	router.post('/users', (req, res) => {

		const name = req.body.name;
		const email = req.body.email;
		const password = req.body.password;
    const phone = req.body.phone;

		if (!name || !email || !password || !phone || !name.trim() || !email.trim() || !password.trim()) {

			res.status(400).json({message: 'Invalid Request !'});

		} else {

			register.registerUser(name,phone, email, password)

			.then(result => {

				res.setHeader('Location', '/users/'+email);
				res.status(result.status).json({ message: result.message })
			})

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});

	router.get('/users/:id', (req,res) => {

		if (checkToken(req)) {

			profile.getProfile(req.params.id)

			.then(result => res.json(result))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			res.status(401).json({ message: 'Invalid Token !' });
		}
	});

	router.put('/users/:id', (req,res) => {

		if (checkToken(req)) {

			const oldPassword = req.body.password;
			const newPassword = req.body.newPassword;

			if (!oldPassword || !newPassword || !oldPassword.trim() || !newPassword.trim()) {

				res.status(400).json({ message: 'Invalid Request !' });

			} else {

				password.changePassword(req.params.id, oldPassword, newPassword)

				.then(result => res.status(result.status).json({ message: result.message }))

				.catch(err => res.status(err.status).json({ message: err.message }));

			}
		} else {

			res.status(401).json({ message: 'Invalid Token !' });
		}
	});

 	router.post('/users/:id/logout', (req, res) => {
    
    if (checkToken(req) && req.params.id.length>0 ){
      const credentials = req.params.id;

      if (!credentials) {
			  res.status(400).json({ message: 'Invalid Request !' });
		  } 
      else {
        logout.logoutUser(credentials)
        .then(result => {
          res.status(result.status).json({ message: result.message, token: token });
        })
        .catch(err => res.status(err.status).json({ message: err.message }));
      }
    }
    else{
      res.status(401).json({ message: 'Invalid Token !' });
    }
	});

	router.post('/users/:id/password', (req,res) => {

		const email = req.params.id;
		const token = req.body.token;
		const newPassword = req.body.password;

		if (!token || !newPassword || !token.trim() || !newPassword.trim()) {

			password.resetPasswordInit(email)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			password.resetPasswordFinish(email, token, newPassword)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }) );
		}
	});

	function checkToken(req) {

		const token = req.headers['x-access-token'];
		if (token) {

			try {

  				var decoded = jwt.verify(token, config.secret);
          console.log(decoded.message)
          console.log(req.params.id)
          console.log(decoded.message === req.params.id)
  				return decoded.message === req.params.id;

			} catch(err) {

				return false;
			}

		} else {

			return false;
		}
	}
}

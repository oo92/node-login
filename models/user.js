'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = mongoose.Schema({ 

	name 			: String,
	email			: {type: String, unique: true}, 
  phone : {type: String, unique:true},
	hashed_password	: String,
	created_at		: String,
	temp_password	: String,
	temp_password_time: String,
  status  : Number,
  isOnline : Number,
  credit : Number
	
});

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://xborder_access_client:xb0rd3r_x_clients@139.59.237.25:2777/NaingTest');

module.exports = mongoose.model('users', userSchema);        

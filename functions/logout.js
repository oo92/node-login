'use strict';

const user = require('../models/user');

exports.logoutUser = email => 

	new Promise((resolve, reject) => {

      user.find({ email: email })
    
      .then(users => {
          let user = users[0];
          user.isOnline= 0;
          user.save();
          user => resolve({ status: 200, message: 'Logged out !' })
		})

		.catch(err => reject({ status: 500, message: 'Internal Server Error !' }));
});




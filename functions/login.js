'use strict';

const user = require('../models/user');
const bcrypt = require('bcryptjs');

exports.loginUser = (email, password) => 

	new Promise((resolve,reject) => {

		user.find({email: email})

		.then(users => {

			if (users.length == 0) {

				reject({ status: 404, message: 'User Not Found !' });

			} else {

				return users[0];
				
			}
		})

		.then(user => {
			const hashed_password = user.hashed_password;

			if (bcrypt.compareSync(password, hashed_password) && user.status ==1) {
				resolve({ status: 200, message: email+":"+user.name+":"+user.phone });

			} else {
        if (user.status == 0){
          reject({ status: 401, message: 'User not verified yet!  Please SMS <activate> to +6591333107 with your username.' });
        }
				reject({ status: 401, message: 'Invalid Credentials !' });
			}
		})

		.catch(err => reject({ status: 500, message: 'Internal Server Error !' }));

	});

	
